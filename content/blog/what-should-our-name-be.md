---
title: "What should our name be?"
date: 2021-04-30T11:16:58+02:00
image : "images/munich-kolar-unsplash.jpg"
author : ["Matthias"]
categories: ["Engagement"]
tags: ["engagement", "project meeting"]
# description: "This is meta description"
draft: false
---

### Dear community,

after months of preparations behind the scenes (of course, it took longer than expected!) the Alumni Network initiative is soon ready to fully launch as an association. **Now we need you and your ideas for our future name!**

<div class="wp-block-button aligncenter"><a name="cta-button" class="wp-block-button__link" href="https://www.menti.com/aa8k2t8df8">👉 Share your ideas</a></div>

We want to build a house for the MCTS Family – the current and incoming graduates, PhD students, and alumni – to meet, exchange and stay in touch. We want to give us all a platform to cherrish our connection, and continue our conversations how to make an impact in the world with our perspectives from Science & Technology Studies and beyond. This should be reflected in the name we choose for our association. *Please note, that we cannot use MCTS or TUM in the name for legal reasons.*

#### How we continue from here

1. You can input your ideas in this mentimeter until Sunday, May 2, 23:59.
2. We will discuss and **decide on the final name in our next project meeting on May 6, 19:30**. We explicitly invite you all to join us for this session on [Zoom](https://tum-conf.zoom.us/j/64546262297?pwd=T1Z6RCtVanZFK2VLZG9GNllUbGhYQT09). Mark it in your calendar or just [download the invite for your calendar](/files/Fantastical-20210430-120206.ics).

We look forward to your ideas and seeing you on May 6 for deciding on the name of our association! 🙂
