---
title: "Christmas Party: Moving on"
date: 2020-12-31T18:57:28+01:00
image : "images/wonder-me-background.jpg"
author : ["Matthias"]
categories: ["Events"]
tags: ["christmas", "event"]
# description: "This is meta description"
draft: false
---

## Student initiative is keen to establish an Alumni Network at the MCTS

*Last week we left the demanding year of 2020 behind us and greeted the new year. We are especially excited about 2021 because we have an ambitious plan. Who are ‘we’, you ask?*

We are nine PhDs, master students and alumni who believe that the people of the MCTS need a space to connect beyond university. That is why we are keen to establish an Alumni Network as a legal association (German: “eingetragener Verein”) by March 2021. Activities that we aim to tackle include: 
- organisation of a yearly “homecoming” for alumni
- talks between alumni and students
- (panel discussions with companies)
- guide to help explain STS and its relevance to future employers 

In fact, we have already realised our first event: a Christmas Party for the entire MCTS community. Over 60 participants joined on Friday evening of the 18th December. All groups of the MCTS were represented, ranging from first and final year students, to staff and alumni. Instead of a usual Zoom call, we tried the platform wonder.me that allowed for more spontaneous conversations. This switch of technology (or shall we call it non-human agent?) paid off. Many commented that they were finally able to have the open and surprising conversations again that are characteristic for the MCTS. Thanks for this feedback. We are happy that so many celebrated online until late into the evening. 

After this success, we are ready to move on – motivated to bring the Alumni Network to life. You want to support us? Amazing! Shoot us a message at alumni-network@mcts.tum.de or visit the website www.sts-munich.de.