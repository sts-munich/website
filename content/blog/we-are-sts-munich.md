---
title: "We are STS Munich, the Students & Alumni Network"
date: 2021-05-20T09:58:00+02:00
image : "images/sts-munich-logo-v1.png"
author : ["Matthias"]
categories: ["Administrative"]
# tags: ["work", "day"]
# description: "This is meta description"
draft: false
---

Dear community,

Thanks for all your suggestions and engagement for the future name of this network! Following your valuable input, the name of our association will be **STS Munich – Students & Alumni Network**.

The first part, *STS Munich*, highlights our connection to the field of Science and Technology Studies and our home university, the Technical University of Munich (TUM). Also, it is alluding to something like a "Munich School" of STS that the coming years may bring to the fore.

The second part, *Students & Alumni Network*, underscores our ambition to be a community for both current graduate and PhD students *and* alumni of all STS-related programmes.

Please [join our group on LinkedIn](https://www.linkedin.com/groups/12545169/) to help us grow strong connections there.
