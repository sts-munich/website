---
title: "We want you for Student Voices!"
date: 2022-04-23T18:54:17+02:00
image : "/images/student-voices-blog-post.png"
author : ["Matthias"]
categories: ["Community Advocacy", "Student Voices"]
# tags: ["work", "day"]
description: "Transforming the community's blog Student Voices together"
draft: false
---

Community advocates, we're looking for you! Let's transform the community's blog **Student Voices** together.

Currently, Student Voices is in an inter-generational transformation process — and we at STS Munich would like to support the smooth handover to a new team that is eager to take it further. Please [write us an email](mailto:team@sts-munich.de) if you are interested to hear more.

Student Voices is a community-run blog for students to publish their academic output, may it be essays, presentations or podcasts. In the past, there has been a particularly productive collaboration with Prof. Ruth Müller's class on *Telling Responsible Stories — Telling Stories Responsibly* in which students created fantastic podcasts themselves. It is important to give students an accessible and safe space to practice publishing to an audience beyond the seminar room. Look [here at what previous generations of students have created](https://www.mcts.tum.de/studium/student-voices/#1596032027482-217bd460-4201).

We at STS Munich are committed to be a platform for student initiatives, helping them with the tedious administrative and infrastructural stuff, so they can focus on making their work shine. Student Voices will have its own space on this website (see [here](/student-voices)), a Slack channel, email address and many other things that our association can help to provide.

Let us know if you're interested to hear more. We're looking for awesome, curious, and committed advocates for this community through taking over this fantastic project 🙂

<div class="wp-block-button aligncenter"><a class="wp-block-button__link"
    href="mailto:team@sts-munich.de">I'm interested, let me know more!</a>
</div>