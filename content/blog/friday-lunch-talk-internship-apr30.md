---
title: "Friday Lunch Talk: Internship"
date: 2021-04-27T21:13:34+02:00
image : "/images/friday-lunch-talk-internship-apr30.png"
author : ["Matthias"]
categories: ["Events"]
tags: ["events", "Friday Lunch Talk Series"]
# description: "This is meta description"
draft: false
---

**Update (13:40): Thanks everyone for attending today! A recommendation from the chat: check the website of the [TUM Career Service for application tips & opportunities](https://www.community.tum.de/en/career-service/).**

We're more than happy to invite you to the first instalment of our Friday Lunch Talk Series on [April 30 at 12:30 pm over at Zoom](https://tum-conf.zoom.us/j/65339098798?pwd=TzRUSVdsTHJVOTZtK3FUdFZBQ01JZz09).

Internships are not only at the core of the RESET curriculum. All STS students can benefit from new insights into organisations at the science-technology-policy-society interface. Beyond gaining relevant work experience an internship may yield important empirical data for future research (think of your thesis!). Also, it may allow you to see different STS themes at play outside university classes.

We've invited several final year students to share their internship experiences, from the application process up to their final reflection. This will also include the specific challenges of international students. After an initial input of each of them, there will be plenty of room to ask questions, first in a larger and later in breakout rooms.

Looking forward to seeing you all on [Zoom](https://tum-conf.zoom.us/j/65339098798?pwd=TzRUSVdsTHJVOTZtK3FUdFZBQ01JZz09).

*P.S.: Don't forget to have look at our [Internship Database spreadsheet](https://nextcloud.sts-munich.de/index.php/s/yEtBy2ZrntrQMzm) if you're still looking for an opportunity.*
