---
title: "STS Dept. Christmas Party 2023"
date: 2023-12-07T10:50:59+01:00
image : "/images/christmas-sts-image.jpg"
author : ["Adeyinka"]
categories: ["Events", "Christmas"]
tags: ["STS Department", "Events", "Christmas"]
# description: "This is meta description"
draft: false

---

📆 **December 15**, 2023 – starting at **7:00 pm** (*open end!) <br>
📍  Department Building (Augustenstrasse 44-46)

***

#### Dear students, alumni, instructors, department staff, and colleagues,

We are happy to announce the Christmas Party of the STS Department 2023!
Like at previous celebrations it is a community-organised event where we will have an International Potluck (for foods and drinks) so let’s everybody bring our favorite Christmas and winter drinks or snacks to share! Glühwein is of course especially appreciated!

Here is the link to a shared spreadsheet where you can sign up for the party, add your potluck contribution, tell us if you have old decoration at home that you would like to donate, list how you can help out, and much more, please check it out [**Registration deadline is Tuesday 05th of December**](https://docs.google.com/spreadsheets/d/1teJAEFo5OXZ-gLyLyaq6RFzUlU_oeDliglT-uxwZwuM/edit).


Add your preferred songs for the karaoke and dancingfloor in our shared spotify playlist: [**Shared Songs and Music - STS Christmas Party**](https://open.spotify.com/playlist/5wuQLEPQjqgr2z9kzHrxGB?si=ZbAtMRSDQVmraWBHDYUgLg&pt=dcebc958367cc9b65e68325016e8b9a1&pi=e-Kql3lmMRTyOV).

Any questions, ideas or technical issues? Send us a brief mail to [team@sts-munich.de](mailto:team@sts-munich.de).

*Your organisation team,*<br>
*Erna Jungstein Rico, Yiğit Ülker, Adeyinka Adebakin, Ariel Khan and Elisenda Passola Lizandra*

***

##### <p style='color:#cd2653'> 🎄 Special Secret Santa Exchange </p>

Participate in the **Special Secret Santa Exchange!** by leaving your full name and email in the same registration link shared above. Afterwards, each participant will be randomly assigned a fellow student to gift. The budget-friendly gifts should be in the 5-10 € range.

***

##### 🧑‍🎄 The Program

###### <p style='color:#0076bb'>7:00 pm — Start of the Christmas Party</p>

**Welcome everyone**

###### <p style='color:#0076bb'>7:30 pm — Opening</p>

After a few warm welcome words from the Department and the community, feel free to discover the event space and join whatever area or conversation you prefer. We've setup various thematic tables / rooms where you can:

- Play **Powerpoint Karaoke**
- Have a drink together over at the **Glühwein Bar**
- Eat cookies over at the **Kitchen** 
- Secert Santa **Gift Exchange**

And of course mix and mingle all around, wonder.me allows you to easily jump from conversations to the next.