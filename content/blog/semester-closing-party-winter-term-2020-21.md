---
title: "Semester Closing Winter Term 2020/21"
date: 2021-03-05T12:33:53+01:00
image : "images/Invitation_WG_Party_2021.jpg"
author : ["Matthias"]
categories: ["Events"]
tags: ["Events", "Winter Term 2020/21", "wonder.me"]
# description: "This is meta description"
draft: false
---

Hi everyone,

To celebrate the end of this winter semester and the submission of all your papers, the MCTS Alumni Network initiative is happy to invite you to a virtual WG Party on **Tuesday, March 16 starting at 8:00 pm**!

We want to revive the German student spirit of mingling at a friend’s apartment (= “WG Party”) – but with a virtual twist. Again, our online meeting space will be wonder.me, and we will send the final link to the room by Monday, March 15. It’s supposed to be a relaxed gathering with drinks, chatters, and fun activities! If you want to help organize a virtual room (games, beer pong, …), feel free to respond to this email.

Stay tuned for more information until then. Have a look here at our website to find out more about our initiative, or simply send us an [email](mailto:alumni-network@mcts.tum.de).

Best wishes and stay healthy! 