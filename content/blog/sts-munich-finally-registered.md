---
title: "STS Munich is finally a registered association (e.V.)!"
date: 2022-04-23T19:21:53+02:00
image : "/images/f91915d3b4b6efaf473c8fc1b2253a31e1df57b13452c08d4389980cfdbe3d16.JPG"
author : ["Admin"]
categories: ["Legal"]
# tags: ["work", "day"]
description: "STS Munich is finally a legally recognised, registered association"
draft: false
---

#### Dear friends

The long wait is over, **STS Munich - Students & Alumni Network** can finally claim to be a fully and legally recognised, registered association by German law ("eingetragener Verein" or abbreviated: "e.V.").

After having founded STS Munich officially on October 8, 2021, just hours before we welcomed former graduates and current students to our very first Homecoming and Graduation Ceremony, the recent mail from the responsible local court in Munich is a huge milestone for us.

It not only allows our association to officially bear the title **e.V.**. We can now, e.g. open a bank account to rightfully receive donations (and, in the future, membership contributions) so we can support student initiatives in shaping our joint research field Science & Technology Studies or host events with invited speakers (and being able to reimburse some of their expenses).

Our motto and mission is threefold:

- **Shape STS**: Studying in the field of Science and Technology Studies will impact your thinking for life. Let's continue to shape this field together.
- **Lean on:** We all have unique backgrounds, skills, and experiences. Let’s build a community in which everyone is setting each other up for success.
- **Meet again:** The best way to flourish in personal and professional life are great conversations. Let’s create opportunities to meet each other, again and again.

We're excited for the all that lays ahead of us and to build a house for the STS family with you.

Best wishes from the Management Board,

*Dominik, Amelie and Matthias*