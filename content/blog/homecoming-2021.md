---
title: "Homecoming on Oct 8-9, 2021 in Munich"
date: 2021-08-18T07:57:18+02:00
image : "images/homecoming-mcts-oct-2021.jpg"
author : ["Matthias"]
categories: ["Events"]
tags: ["events", "homecoming"]
# description: "This is meta description"
draft: false
---

## Dear students, dear alumni, dear MCTS community!

The Students & Alumni Network is thrilled to invite you all to the very **first Homecoming on October 8 to 9 (Fri-Sat) in Munich**! ~~Please let us know by September 20 if you’re coming:~~

We’re planning for a two-day in-person event, starting with a community-organized graduation ceremony, followed by a get-together and a day-long activity around Munich on Saturday. We will **start on Friday at 5 pm** and **end on Saturday between 3 and 4 pm**. The event will, of course, be subject to the latest regulations and a hygiene concept.

We had challenging digital semesters and time; it’s time to come together to celebrate our achievements and a return to in-person university. Stay tuned for more in the upcoming weeks (helping hands will surely be appreciated at some point!). You are also welcome to join our new [LinkedIn group](https://www.linkedin.com/groups/12545169/)!

Any questions? Reply to [team@sts-munich.de](mailto:team@sts-munich.de). ~~Let us know if you can make it:~~

Best wishes from
Amelie, Matthias & the STS Munich team