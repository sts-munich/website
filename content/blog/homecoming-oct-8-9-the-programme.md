+++
author = ["Matthias"]
categories = ["Events"]
date = 2021-10-04T13:00:00Z
image = "/uploads/minna-thiel-munich.jpg"
tags = ["Program", "Homecoming", "Events"]
title = "Homecoming 2021 – The Program [updated]"

+++
## Dear community

Our 1st Homecoming on October 8-9, 2021 is drawing closer. We're counting more than 60 sign-ups! ~~Please register here **by September 20** at the latest, also if you like to bring a +1.~~

This is to inform you about the program for the two days and any open questions you might have.

***

### 👉 The Program (short)

##### Friday, October 8

* **4:30 pm** — Opening the doors @ **Friedrich-von-Thiersch lecture hall** ([Arcisstrasse 21, Stammgelände, see here](https://www.ph.tum.de/about/visit/roomfinder/?room=0503.02.300&language=en)) for Graduation Ceremony
* **5:00 pm** — Opening speech by Sebastian Pfotenhauer
* **5:15 pm** — Main part of the Graduation Ceremony
* **7:00 pm** — Get-together with food and drinks @ **Minna Thiel** (in front of the HFF, Bernd-Eichinger-Platz 1, 80333 München)

##### Saturday, October 9

* **9:50 am** — Trip to Ammersee and Kloster Andechs, meeting at Munich Central Station (S8 to Herrsching, track 2)
* **3 pm** — end of Homecoming

***

### The Program (long)

#### Friday, Oct 8, 5-7 pm — Graduation Ceremony

We will open the doors for you to the **Friedrich-von-Thiersch lecture hall** ([Arcisstrasse 21, Stammgelände](https://www.ph.tum.de/about/visit/roomfinder/?room=0503.02.300&language=en)) at 4:30 pm. Please wear a facemask and check-in at a fixed seat as soon as the event formally starts.

**Sebastian Pfotenhauer and Ruth Müller** will kick off the self-organized Graduation Ceremony with a few opening remarks (_5 pm_).

**Afterwards, we will honour our graduates** of all programs (_5:15 pm_) _who have handed in their thesis or formally graduated in the past years or days before October 7, 2021_. We invite each graduate individually up to the front to receive their round of applause and briefly tell us about their journey at MCTS, including a mention of their thesis, and where they are heading now (or have been since). A photographer will make sure your special moment is captured accordingly!

**We will commence** the Graduation Ceremony with a few words on the future of the _STS Munich_ association (_6:15 pm_).

#### Friday, Oct 8, after 7 pm — Get-Together at Minna Thiel

![](/uploads/minna-thiel-16-9.jpg "Courtesy of Sabine Schwimmbeck")

We have reserved several tables under the tents at the outdoor venue [Minna Thiel](https://www.instagram.com/minna.thiel/), the good wife of the famous [Bahnwärter Thiel](https://www.bahnwaerterthiel.de/). It is in front of the HFF (Hochschule für Fernsehen und Film) right between Königsplatz and Pinakothek der Moderne (Bernd-Eichinger-Platz 1, 80333 München). Grab some easy-going foods and drinks and enjoy being back among old friends and new faces from the community. **Be sure to bring / wear warm clothes!**

_We ask you to cover your own expenses at Minna Thiel._

#### Saturday, Oct 9, 10 am - 3 pm — Ammersee and Kloster Andechs

![](/uploads/luke-porter-neqec7qa9fm-unsplash.jpg "Courtesy of Luke Porter, Unsplash")

We'll make a trip to Ammersee and Kloster Andechs, hoping for a favourable autumn sun! **We'll meet at 9:50 am at Munich Central Station and take the S8 to Herrsching on track 2** (this should all be covered by your MVV student ticket). After a proper lunch at Kloster Andechs' beergarden* between 12:30 and 2 pm, we complete our round and probably arrive at Herrsching station around 3 pm.

_*) Again, we ask you to cover own expenses during the trip._

***

### Frequently Asked Questions (FAQ)

##### I want to come to the Homecoming but haven't signed up by September 20. Can I still join?

To comply with the latest public health regulations and organize the event accordingly, we have to know the number of attendees of our event, including their names, _in advance_. Please write us a message to [team@sts-munich.de](mailto:team@sts-munich.de) and we'll see whether we can still accommodate you.

##### I can't join the Homecoming (or parts of it) anymore. How should I let you know?

Please write us a message to [team@sts-munich.de](mailto:team@sts-munich.de), so we'll have an overview who is coming or not.

##### How does the Graduation Ceremony work? Who will be honoured?

Every graduate of the programs at MCTS, whether it's RESET, STS, WTPhil or a Doctoral program is eligible for a well-deserved, individual round of applause. **For us, graduate defines as everyone who has submitted their thesis any time before October 7, 2021.**

We will invite every graduate individually* up to the front to receive their round of applause and tell us briefly (2-3 min) about their journey at MCTS, a quick mention of their thesis and the way ahead / since.

_*)_ _This is also a way to comply with indoor public health restrictions._

##### Why should I bother mentioning my Thesis at the Graduation Ceremony?

Putting together the final thesis is not just a major milestone and academic achievement. Our aim is to foster intellectual exchange among graduates and soon-to-be graduates about their academic work and aspirations. What better way could there be to learn what others have written or thought about, which literature or methodology helped them most!

With your consent, we include your thesis in [this padlet](https://padlet.com/stsmunich/ecz4xppfbo9ph1o), showcasing all submitted thesis, including the title and abstract, while providing the author's name is entirely optional.

##### Is there a dress code at the Graduation Ceremony?

No, there isn't. Come as you feel comfortable. However, you're more than invited to come and dress up in a more festive way. You and / or your classmates are celebrated for their graduation and achievements! **Just be sure to also bring some warm clothes to throw over for the Get-together afterwards.**

##### Are there vegan or vegetarian food options at the Get-Together?

Yes! Minna Thiel, where we'll be for the Get-Together, will be catered by the foodtruck [Chefbox](https://www.chefbox-augsburg.de/) that offers a variety of options.

##### Do I have to pay for food and drinks during the event myself?

While the Munich Center for Technology in Society has generously pledged financial and administrative support for the Graduation Ceremony, you will have to pay for your expenses at the Get-Together and the following day yourself. As of now, STS Munich does not any financial resources to sponsor these expenses.

##### Can I bring my +1 to the event?

Yes, you can but only if you have let us known through our sign-up form.

##### Do you require proof of vaccination, recovery or current test result?

Yes! We will apply the **3G concept**. Please show us your proof of complete vaccination, recovery or current test result (no later than 24 h) upon arrival at any of our activities. Otherwise, we won't be able to accommodate you.

##### How do you cover any expenses for the Homecoming?

While MCTS has generously pledged financial support, everything else is paid thanks to contributions by members of _STS Munich_ — or you! We are happy for any further, ever so tiny contribution 🙂

##### How can I make sure there will be a Homecoming next year, too?

You can join _STS Munich_ as a member — no worries, that first of all means you are paying member of a (charitable) association, no other strings attached. If you're becoming a member you can, of course, help to organise the Homecoming next year.

##### Can I join _STS Munich_ as a member?

Yes, you can. We will officially establish the association right before the event and will provide opportunities to sign-up for membership (no worries, that's like a club membership and doesn't mean you have to participate in any organizational matters) throughout and on this website.
