---
title: "Meet the new STS Munich board members!"
date: 2023-05-31T17:09:42Z
image : "/images/board_member.jpg"
author : ["Admin"]
categories: ["Administrative"]
# tags: ["work", "day"]
description: " Welcome the newly appointed board members"
draft: false

---
#### Attention all STS Munich members
It's time to break out the confetti and champagne! We're excited to introduce our newly elected board of directors who are ready to hit the ground running with their new roles.

Let's give a big round of applause to our new board members:

- **Yiğit Ülker**,  1st Chairperson
- **Erna Jungstein Rico**, 2nd Chairperson
- **Adeyinka Adebakin**, Treasurer
- **Elisenda Passola Lizandra**, 4th Board Member 
- **Ariel Khan**, 5th Board Member 

We couldn't be more thrilled to have such a dynamic and diverse group of alumni leaders to help guide us towards even greater success. Their fresh perspectives and innovative ideas are sure to take our alumni association to the next level.

To our outgoing board members: THANK YOU for your hard work and dedication to our community. We couldn't have gotten this far without you, and we know you'll continue to make us proud in everything you do. 

Stay tuned for updates on upcoming events and opportunities to get involved with us! Let's raise a virtual glass to our new management board and all the exciting things to come! 

Cheers,

*STS Munich*
