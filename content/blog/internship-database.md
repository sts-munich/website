---
title: "Looking for an internship? Sharing is caring!"
date: 2022-05-31T20:39:35+02:00
image : "images/john-schnobrich-2FPjlAyMQTA-unsplash.jpg"
author : ["Matthias"]
categories: ["Network"]
tags: ["internships", "database","sharing"]
# description: "This is meta description"
draft: False
---

Whether it is a mandatory part of your degree or not, gaining practical experiences through internships in industry, public sector, consultancy, international development or any sector is tremendously helpful.

That's why we've started to collect tips and experiences from members of our community to help you during your search for a suitable and hopefully rewarding internship experience. Please [check out our database over here](https://cloud.seatable.io/dtable/external-links/9a697ab0002249d0a837/) or hop over to the dedicated [Internships page](/internships).

If you've already completed your internship, it would be fantastic if you could [share your experiences with community](https://cloud.seatable.io/dtable/forms/7881a914-41fb-4b9b-a27c-53543db4b083/). Don't worry: if you feel uncomfortable sharing your name alongside, that's perfectly fine. Every bit helps others to navigate the vast and sometimes daunting sea of possibilities.

Thanks so much 🙂