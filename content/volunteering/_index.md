---
title: Volunteering Positions
description: Looking for a volunteering experience?
image: "/images/john-schnobrich-2FPjlAyMQTA-unsplash.jpg"
layout: student-voices

---

<div class="wp-block-button aligncenter"><a class="wp-block-button__link"
    href="#postings">Jump to Positions 👇</a>
</div>

#### Join us in nurturing the network!

You're looking for a volunteering experience close to your academic program? You want to have an impact in your university community? You are excited to build things from scratch; acquire new skills, e.g. in project management, event organisation, social media marketing or process automation; extending our network across Europe and make meaningful connections for yourself on the go? **Then we're looking exactly for you.**

Demonstrated volunteering experiences are highly valued by future employers across private/public sector and academia, but also beneficial (even decisive) in scholarship or grant applications. They also offer you a fantastic chance to test out and acquire new skills. As a newly established network and charitable registered association ("gemeinnütziger eingetragener Verein"), STS Munich e.V. is actively looking for support and offers several volunteering positions, with diverse responsibilities and time commitments. **Your volunteering will certainly be certified and we're happy to write you a short recommendation on request.**

##### Interested? Check the positions below and …

<div class="wp-block-button aligncenter"><a class="wp-block-button__link"
    href="mailto:team@sts-munich.de">Send us a message ✍️</a>
</div>

***

STS Munich's mission is to build a home for the Science, Technology and Society (STS) family, in Munich and beyond. STS Munich e.V.[^abbreviation] is a charitable registered association, grown out of an student initiative at the STS Department of the Technical University of Munich. We're driven by three goals:

- **Lean On** — We all have unique backgrounds, skills, and experiences. Let’s build a community in which everyone is setting each other up for success in professional and personal development.
- **Shape STS** — Studying Science & Technology Studies will impact your thinking for life. Let’s continue to shape this field together and build academic networks across Europe.
- **Meet again** – The best way to flourish in personal and professional life are great conversations. Let’s create opportunities to meet each other, again and again.

[^abbreviation]: The full name of the charitable registered association is *STS Munich - Students and Alumni Network e.V.* See our [About page](/about) and [Legal Notice](/impressum).

***

## Volunteering Positions {#postings}

✅ = still open
🤝 = already closed

The **time commitment / month** figure is just a rough guess of us.

#### <b style='color:#0076bb'>Social Media Lead</b> — LinkedIn, Instagram and/or Twitter ✅

This is an exciting volunteering challenge at the intersection of community advocacy, science communication and a bit of education marketing. Shape the awareness and digital brand of our association among our community, in the academic world and make sure that we're well represented on the relevant social networks. This includes writing snippets and creating visual graphics for our gatherings/offers but also may include promoting STS as an academic discipline.

Use this opportunity to learn about social media marketing and storytelling yourself and visit seminars such as those organized by the [Haus des Stiftens](https://www.hausdesstiftens.org/digital-camp-2022/) in the capacity of our association.

We currently want to focus on LinkedIn (our group and a page) and Instagram. If you wish, you could initiate a Twitter account, too.

time commitment / month: \~12h

Who to talk to for details? 👉 

#### <b style='color:#0076bb'>Project Lead Student Voices</b> — Transforming the community's blog <i>Student Voices</i> 🤝

**Student Voices** is a community-run blog for students to publish their academic output, be it essays, presentations or podcasts. It is important to give students an accessible and in a way safe platform to practice publishing to an audience beyond the seminar room. Student Voices is currently in an inter-generational transformation process. STS Munich has committed to be a platform for student initiatives, helping them with the tedious administrative and infrastructural stuff (communication channel, cloud software, website etc.) and sharing its access to the community.

The Project Lead takes over responsibility for a small group of editors and is also encouraged to regularly call for submissions in the community.

time commitment / month: \~12h

#### <b style='color:#0076bb'> User-Centred Design and Software Lead </b> — Creating a digital-first association ✅

Setting up a digital-first association is an exciting challenge and involves designing a clever 'tech stack' of various web-based software services. Your job would be make the digital experience for our members and interested parties as pleasing as possible, e.g. by automating processes and touch points as much as possible. 

We're using a host of up-to-date (API-enabled) software services such as [Make.com](https://make.com), [SeaTable.io](https://seatable.io) and [SendInBlue.com](https://sendinblue.com) that always benefit from an even better integration. Beyond managing our tech stack, you'd probably work in hand in hand with our Web Developer.

time commitment / month: \~10h

Who to talk to for details? 👉  [Matthias Meller](mailto:m.meller@tum.de)

#### <b style='color:#0076bb'>(Frontend) Web Developer</b> — sts-munich.de ✅

Our website **sts-munich.de** is central to interact with our community, giving them access to diverse network offers such as the Internship or Thesis Database. It continuously needs a bit of caretaking, from updating content or adapting its design. It's all not rocket science — and if you wish to make more out of it, go for it 🙂

Prerequisite for this position is some knowledge of HTML and CSS (you should have had your hands on juggling with some HTML/CSS code once or twice, however no fully fledged webdesign skills are needed!). You should be curious to potentially learn something new to you and not afraid to google your way around to solve that little technical mystery that unnecessarily stands in your way 🙂 Our website is hosted on [GitLab](https://gitlab.com), and built with the web framework / static site builder [Hugo](https://gohugo.io). 

time commitment / month: \~10h

Who to talk to for details? 👉  [Matthias Meller](mailto:m.meller@tum.de)

#### <b style='color:#0076bb'>Event Coordinator </b> ✅

From monthly Stammtische over seminars to the organisation of signature events such as the Homecoming …

time commitment / month: \~12h

#### <b style='color:#0076bb'>International Relations Manager </b> ✅

A core aim of us is to extend the network beyond Munich and build connections with communities elsewhere across in Europe. We would also like to contribute the reach, impact and richness of experiences of our network of students and alumni. Your job could be building relationships with STS communities in, e.g. Edinburgh, London, Paris or Vienna, with the aim of organising get-togethers or even joint events in the future. Develop a concept from scratch with regular support and feedback from the team.

time commitment / month: \~6h

#### <b style='color:#0076bb'>Community Advocate and Outreach</b> — Newsletter ✅

Write a monthly newsletter, collect interesting articles to share, collect news from the community
prerequisites: happy reading around for interesting academic and non-academic articles to share with the community, interested in keeping in touch with various members and stakeholders of the community, eager to create recurring format foremost for the community but with the potential of a greater reach

time commitment / month: \~8h (3h newsletter composition + 5h collecting material)

#### <b style='color:#0076bb'>Community Manager</b> — Slack, WhatsApp & Co. ✅

As our community grows, we need creative helping hands, moderating our channels of group communication such as Slack / WhatsApp. This person needs to keep a good eye on what's going on in the community and navigate between 

time commitment / month: \~6h
…

#### <b style='color:#0076bb'>Content Creator</b> — Mapping STS in Germany (and Europe) ✅

So, you're interested in an STS PhD but don't know where to apply, besides TUM? We neither. STS is a relatively young discipline and often appears little institutionalised in German (and European) universities. We think this is a problem to be solved: where are all those STS-flavoured institutes, would-be departments and programs? Mapping them out not only helps to clarify the STS landscape and may serve as inspiration where your academic career may move next. It can also be the starting point to connect to other STS communities, fostering cross-university and even cross-national bonds, serving as a platform for future exchanges.

time commitment / month: \~6h

#### <b style='color:#0076bb'>Content Curator</b> – "Essential STS Reader" ✅

Academic content for and from the community

time commitment / month: \~6h