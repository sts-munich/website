---
title: "Statutes of Association"
# meta description
description: "Statutes of Association"
# save as draft
draft: false
layout: staticpage
---

<div class="wp-block-button aligncenter"><a class="wp-block-button__link"
    href="https://go.sts-munich.de/join-us">Become a member</a>
</div>

***

Thank you for your interest in our registered association ("e.V." or "eingetragener Verein") *STS Munich - Students and Alumni Network e.V.".

Please see below the legally binding **Statutes of Association** that govern our association, in effect since October 8, 2021. Currently, we only provide the German original. However, we are working on an English translation. 

Furthermore, please see our association's **Privacy Policy** (distinct from this website's Privacy Policy) that you need to accept when you become a member of STS Munich. The current and binding version of the Privacy Policy is also only provided in German, but we are working on an English translation, too.

##### Statutes of Association

[Download](https://go.sts-munich.de/statutes)

##### Privacy Policy

[Download](https://go.sts-munich.de/privacy-policy)