---
title: "STS Dept. Christmas Party 2021"
# meta description
description: "A few notes on the STS Dept. Christmas Party"
# save as draft
draft: true
layout: staticpage
---

📆 **December 16**, 2021 – starting at **7:30 pm** <br>
📍 Our [virtual Christmas Party space](https://app.wonder.me/?spaceId=83f5d8ef-99fb-453b-9953-d92455f20d18) on wonder.me (*does not support mobile devices!*)

<div class="wp-block-button aligncenter"><a name="save-to-calendar" class="wp-block-button__link" href="/files/STS-Dept-Christmas-Party-2021.ics">Save to calendar</a></div>

***

#### Dear students, alumni, instructors, department staff, and colleagues,

A warm welcome to this year's STS Dept. Christmas Party. This is to give you a quick overview what's planned plus a brief how-to for using the tool [wonder.me](https://wonder.me). Any questions, ideas or technical issues? Send us a brief mail to [team@sts-munich.de](mailto:team@sts-munich.de).

*Your organisation team,*<br>
*Nadine Osbild, Amelie Anders, Matthias Meller, Clara Valdés Stauber and Dominik Wedber*

##### 🎄 Joining the Christmas Party

Open this link to enter our Christmas Party online venue: [https://app.wonder.me/?spaceId=83f5d8ef-99fb-453b-9953-d92455f20d18](https://app.wonder.me/?spaceId=83f5d8ef-99fb-453b-9953-d92455f20d18)

You don't need a password. Please **use either Chrome, Firefox or Edge** browser on your computer. [wonder.me](https://help.wonder.me/en/articles/5622483-supported-devices-browsers) does not support tablets or smartphones.

***

##### <p style='color:#cd2653'>"Backup" Zoom-Meeting</p>

In case wonder.me should fail for any reason we have setup a separate Zoom meeting as a fallback option. You can join without waiting for any host.

👉 Join here: [https://tum-conf.zoom.us/j/65189806372?pwd=ODRBbDlaUDRRWVN5UG10Q0UvaGQrUT09](https://tum-conf.zoom.us/j/65189806372)

***

##### 🧑‍🎄 The Program

###### <p style='color:#0076bb'>7:30 pm — Start of the Christmas Party</p>

![Welcome everyone](/images/Christmas-Party-2021/wonder.me-welcome.png)

###### <p style='color:#0076bb'>7:45 pm — Opening</p>

After a few warm welcome words from the Department and the community, feel free to discover the event space and join whatever area or conversation you prefer. We've setup various thematic tables / rooms where you can:

- Play **Powerpoint Karaoke**, organised by Cansu Güner, PhD Candidate at MCTS
- Have a drink together over at the **Glühwein Bar**
- Eat cookies over at the **Kitchen** (there's a balcony for our smokers …)

And of course mix and mingle all around, wonder.me allows you to easily jump from conversations to the next.