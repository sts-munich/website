---
title: "Christmas Party 2020"
# meta description
description: "A few notes on the MCTS Community Christmas Party"
# save as draft
draft: true
layout: staticpage
---

#### A Warm Welcome everyone to 2020's MCTS Community Christmas Party!

This is to give you a quick overview what's planned plus a brief how-to for using the tool <a href='https://wonder.me'>wonder.me</a>. If you run into any technical issues or have other relevant queries, please send us a quick mail over to <a href='mailto:alumni-network@mcts.tum.de'>alumni-network@mcts.tum.de</a> or ask for <i>Matthias Meller</i>, <i>Amelie Anders</i> or <i>Nadine Patzelt</i>.

##### 🎄 Joining the Christmas Party

Open this link to enter our Christmas Party online venue: wonder.me. <br><br>You don't need a password. Please <b>use either Chrome or Firefox</b> (on a desktop computer).<br><br>📆  Want to add the event to your digital calendar to get automatically get a reminder and have the link ready? Download the <a href='/alumni-network/files/2020-12-18_MCTS Community Christmas Party.ics'>.ics file here</a>. Also, here is the <a href='/alumni-network/files/2020-12-18_Christmas Party_Emergency Zoom.ics'>.ics file to the Zoom emergency meeting</a>.

***

##### <p style='color:#cd2653'>"Emergency" Zoom-Meeting</p>

In case wonder.me should fail for any reason we have setup a separate Zoom meeting as a fallback option. You can join without waiting for any host. We will try to look in between to make sure everyone was able to join wonder.me. <br><br>
<b>Join Zoom meeting:</b><br>
<b>Password:</b> welcome!

***

##### 🧑‍🎄 The Program

###### <p style='color:#0076bb'>7:30 pm — What happens to MCTS graduates when they "grow up"?</p>

<i>Join us over at the <b>Alumni Talk</b> table!</i>

- We'll briefly introduce the MCTS Alumni Network Initiative
- Three STS & RESET alumni share a bit of their journeys since graduation
- <b>Your opportunity to ask questions!</b>

###### <p style='color:#0076bb'>8:00 pm — Opening of MCTS Christmas Party</p>

After a few warm welcome words by the MCTS community, feel free to discover the event space and join whatever area or conversation you prefer. We've setup various thematic tables / rooms where you can:

- Play <b>Powerpoint Karaoke</b>, organised by Cansu Güner, PhD Candidate at MCTS
- Have fun with Games designed for online meetings, hosted by Oliver Zillig, STS Master student
- Share your work and internship experience at <b>Work Experience & Co.</b>
- Have a drink together over at the <b>Glühwein Bar</b>
- Join the <b>STS Talk</b>
- Discuss your <b>New Year's Resolutions</b>
— Eat cookies over at the <b>Kitchen</b> (there's a balcony for our smokers …)

And of course mix and mingle all around, wonder.me allows you to easily jump from conversations to the next.

##### How-To: Wonder.me

We're using the tool <a href='https://wonder.me'>wonder.me</a>, a Germany-based tool for creating virtual meeting spaces. Please review its <a href='https://www.wonder.me/privacy-policy'>privacy policy</a> (and <a href='https://support.wonder.me/hc/en-us/articles/360013622577-Privacy-Security-FAQs'>FAQ</a>) prior to joining.

###### 1. Join link, enable / check camera and microphone

Again, make sure to use Chrome or Safari, otherwise some features of wonder.me won't work. No worries, you also don't have download anything, it's all browser-based. Be sure to have a decent internet connection and connect your computer to power.

![Enable camera and microphone](/alumni-network/images/01-wonder-enable-camera.png)

###### 2. Enter your name and take a nice photo

The photo is your avatar in wonder.me that allows others to spot you in our Christmas Party venue. Make sure to look stellar 😊

![Enter your name, take a photo](/alumni-network/images/02-wonder-type-name.png)

###### 3. Welcome to the Christmas Party space

Make yourself at home, be sure everything works for you.

![Overview](/alumni-network/images/03-wonder-overview.png)

###### 4. Zoom in to explore …

… who else is around and what tables or conversations you could join.

![Zoom in](/alumni-network/images/04-wonder-zoom-in.png)