---
title: Thesis Database
description: The STS Munich Thesis Database
image: "images/theses-database.jpg"
layout: student-voices

---

#### Sharing is Caring: Introducing the STS Munich Thesis Database

This database allows you to record your master's or PhD thesis, with or without sharing the file as PDF, anonymously or fully disclosed so interested members of the community can get in touch to discuss and learn from your work.

It has been a dear wish of us from the start to map the diverse STS research in our community. Our research skills mature with mutual exchange and learning, not to forget it makes so much more fun and creates a sense of belonging. In academia, we all stand on the shoulder of giants and we should acknowledge our intellectual foundations and inspirations — and pass them on. 

{{< button-large-blue rel-url="https://cloud.seatable.io/dtable/forms/4c5f1869-f559-437a-9047-a86aef5d0346/" title="👉 Submit my thesis" >}}

*If you share a PDF, please make sure that the file does not exceed 8 MB and remove/blacken all sensitive personal data (e.g., matriculation number …).*

***

<iframe className="dtable-embed" src="https://cloud.seatable.io/dtable/view-external-links/e561ba061c5846a0b805/" frameBorder="0" width="100%" height="667" style="background: transparent; border: 1px solid #ccc;"></iframe>