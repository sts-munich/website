---
title: "Virtual WG Party"
# meta description
description: "A few notes on our Virtual WG Party"
# save as draft
draft: true
layout: staticpage
---

#### Let's start off the semester break with our Party!

This is to give you a quick overview what's planned plus a brief how-to for using the tool [wonder.me](https://wonder.me). If you run into any technical issues or have other relevant queries, please send us a quick mail over to [alumni-network@mcts.tum.de](mailto:alumni-network@mcts.tum.de) or ask for <i>Matthias Meller</i>, <i>Amelie Anders</i> or <i>Nadine Patzelt</i>.

##### 🥳 Joining the virtual WG Party

Open this link to enter our WG Party online venue: [https://www.wonder.me/r?id=02418b2b-e994-4a0c-85b8-dc060e700347](https://www.wonder.me/r?id=02418b2b-e994-4a0c-85b8-dc060e700347). <br><br>You don't need a password. Please <b>use Chrome or Microsoft Edge</b> on your computer. Firefox and mobile phones unfortunately are no longer supported.

***

##### 🎉 The Program

###### <p style='color:#0076bb'>8:00 pm to open-end </p>

After a few quick updates from the Alumni Network initiative, feel free to mix and mingle and join thematic tables to …

- play [Skribbl.io](https://skribbl.io/),
- or rounds of [Codenames](https://codenames.game/),
- or [GarticPhone](https://garticphone.com/en) (remember *Stille Post*?),
- or join us over at the **cocktail kitchen**. We have also uploaded a few [cocktail inspirations](/alumni-network/files/2021-03-16_WG-Party_Cocktails.pdf) for you.

Beyond, wonder.me allows you to easily jump from conversations to the next.

##### How-To: Wonder.me

We're using the tool <a href='https://wonder.me'>wonder.me</a>, a Germany-based tool for creating virtual meeting spaces. Please review its <a href='https://www.wonder.me/policies/privacy-policies'>privacy policy</a> (and <a href='https://support.wonder.me/hc/en-us/articles/360013622577-Privacy-Security-FAQs'>FAQ</a>) prior to joining.

###### 1. Join link, enable / check camera and microphone

Again, make sure to use Chrome or Microsoft Edge. No worries, you also don't have download anything, it's all browser-based. Be sure to have a decent internet connection and connect your computer to power.

![Enable camera and microphone](/alumni-network/images/01-wonder-enable-camera.png)

###### 2. Enter your name and take a nice photo

The photo is your avatar in wonder.me that allows others to spot you in our virtual WG Party venue. Make sure to look stellar 😊

![Enter your name, take a photo](/alumni-network/images/02-wonder-type-name.png)

###### 3. Welcome to the WG Party space

Make yourself at home, be sure everything works for you.

![Overview](/alumni-network/images/03-wonder-overview.png)

###### 4. Zoom in to explore …

… who else is around and what tables or conversations you could join.

![Zoom in](/alumni-network/images/04-wonder-zoom-in.png)