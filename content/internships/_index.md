---
title: "Internship Database"
# meta description
description: "Check out STS Munich's Internship Database"
# save as draft
draft: false
image: "/images/john-schnobrich-2FPjlAyMQTA-unsplash.jpg"
layout: student-voices
---

Whether it is a mandatory part of your degree [Responsibility in Science, Engineering and Technology](https://www.mcts.tum.de/en/programs/master-reset-2/) or not, gaining practical experiences in industry, public sector, consultancy, international development or any sector is tremendously helpful. You may gain a totally new perspective on the relevance of your degree, will certainly build your skillset and make important experiences for your future career choices. 

STS Munich is here is to support you in finding an internship through curating a database of our network's past internship experiences. True to our value of "Lean On", let's leverage our unique backgrounds, skills and experiences, and set each other up for success.

##### Are you still looking for an internship? See below!

<div class="wp-block-button aligncenter"><a class="wp-block-button__link"
    href="https://cloud.seatable.io/dtable/external-links/9a697ab0002249d0a837/">See Internship Database</a>
</div>


##### You've already done an internship? 

<div class="wp-block-button aligncenter"><a class="wp-block-button__link bg-accent"
    href="https://cloud.seatable.io/dtable/forms/7881a914-41fb-4b9b-a27c-53543db4b083/">Share internship experience</a>
</div>
