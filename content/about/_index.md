---
title: "About us"
# meta description
description: "Find out about the association and the team"
# save as draft
draft: false
---

Welcome to the website of **STS Munich – Students & Alumni Network**. We are a network for Science & Technology Studies (STS) scholars, to promote the public dialogue on the role of science, technology and innovation in society. We are a registered association ("eingetragener Verein", short: e.V.) based in Munich, founded by students and alumni of the TUM School of Social Sciences & Technology in October 2021.

#### What we've achieved already

The first idea to create a network for students and alumni from the Department of Science, Technology and Society at TU Munich was born in March 2020. One and half years, a virtual Christmas Party and the first Homecoming later, [we've founded](https://www.instagram.com/p/CV6DIZiNy7x/) our association in October 2021.

[![Our Timeline](/images/2021-11-05_STS-Munich_Timeline-trimmed.png "Click to enlarge")](/images/2021-11-05_STS-Munich_Timeline-trimmed.png)

#### Our Management Board

In our founding assembly in October 2021 we elected three of our founding members to the **Management Board** of *STS Munich – Students & Alumni Network e.V. (i.Gr.)*. The Management Board is responsible for leading the association for the duration of their term (usually one year). Currently, [Dominik Wedber](https://www.linkedin.com/in/dominik-wedber/), [Amelie Anders](https://www.linkedin.com/in/amelie-anders/) and [Matthias Meller](https://www.linkedin.com/in/mameller/) (from left to right) serve on the Management Board.

![Our Management Board](/images/f91915d3b4b6efaf473c8fc1b2253a31e1df57b13452c08d4389980cfdbe3d16.JPG)

#### What is an registered association?

A registered association ("eingetragener Verein", or abbreviated: e.V.) is a defined term in German civil code. [Deutsche Welle](https://www.dw.com/en/get-to-know-the-concept-of-the-german-verein/a-48306152) has a nice explainer on its general concept. Basically, it allows initiatives, clubs or any kind of groups to "get organized" formally for and around a specific purpose. This includes the recognition by the German state as legal entity (somewhat comparable to the status of a company) that comes with several legal and administrative benefits. If coupled with a recognition as charitable organisation ("gemeinnützige Organisation"), a registered association may accept donations tax-free and generally largely enjoys tax exemptions in its activities.

The most important body in a registered association is the **General Assembly**, composed of the members of the association. It votes and decides on the rules which govern the association (**Statutes of Association**). Also, it elects the **Management Board** that has the executive responsibility to lead the association for the duration of their term (usually one year). See the infographic below how the different bodies and pieces of an association work together.

[![Infographic Association](/images/2021-11-05_STS-Munich_Association_Explainer.001.png "Click to enlarge")](/images/2021-11-05_STS-Munich_Association_Explainer.001.png)

<!-- ### The Vision

We believe students and PhDs at Munich Center for Technology in Society, whether past or present, need a space to connect beyond university and bring the best of STS into the world.

That’s why we started this initiative for an alumni network.

### The Mission

Our goal is to found an association (“eingetragener Verein”) to enable students and PhDs at Munich Center for Technology in Society to stay connected, for career orientation, peer-to-peer networking, living the MCTS spirit and so much more.

### Possible Activities

We could imagine a range of activities such as:

- Organising an alumni homecoming
- Establishing career mentoring
- Publishing a job market
- Put together essential readings in STS for our first years
- Foster professional exchange
- Share stories from the network through newsletters and social media
- Hold Best Thesis or Essay Awards
- and so much more!

### Who are we?

We are a group of current and former students of the Munich Center for Technology in Society at the Technical University of Munich. We are united by the idea to find ways fostering a long-lasting exchange of alumni, current students and PhDs of the MCTS — basically, how to hold the MCTS family together in the longterm. Inspired by the successful example of an alumni network of our previous studies we started this initiative. <br><br>Please don't hesitate to get in touch with us at [alumni-network@mcts.tum.de](mailto:alumni-network@mcts.tum.de)! -->