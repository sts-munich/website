+++
author = ["Student Voices"]
categories = ["podcasts"]
date = 2022-10-03T23:00:00Z
image = "/uploads/daniel-schludi-mbgxz7pt0jm-unsplash.jpg"
slug = "podcasts"
tags = ["podcasts", "podcast", "responsible storytelling ", "telling responsible stories"]
title = "Podcasts"

+++
## Podcasts

Podcasts today are omnipresent. For many, they have become the preferred, on-demand radio for a morning or afternoon commute. They are everywhere and for good reasons – listening to ideas in the form of interviews and stories can be an effective way to learn and understand a new topic.

For the first time at the STS Department, Professor Ruth Mueller’s seminar “Telling Responsible Stories – Telling Stories Responsibly” drove students to understand the ins and outs of storytelling practices from an STS perspective. By studying storytelling practices, students dove deeper into understanding how sustainable and responsible solutions are presented and constructed in contemporary society. The study of these grand narratives, inspired by work from STS scholars such as Donna Haraway, challenges how stories of solutions can be political, economic, or technological in nature. In the seminar, students were encouraged to rethink practices of storytelling and to tell the “story otherwise” in the narrative genre of podcasts. As the final project for the seminar, students produced their own podcasts, which are available to you here at Student Voices – tune in.

#### Drops of Hope: Making Water in Puebla

What is good innovation? What kind of innovation is it when it comes from outside the Silicon Valley elite? To find innovation outside the walls of Northern California, we changed how we define innovation, who makes it, and where to look for it. That’s how we ended up in Puebla, Mexico. [**Read more…**](https://sts-munich.de/student-voices/drops-of-hope/ "Read more ...")

#### Menstruation Emancipation

Period Poverty is a global issue that sometimes falls into oblivion even though there are numerous people not only menstruating regularly but also suffering the health and social consequences of this problem. To understand the origins and consequences of period poverty we examined the topic in more depth. To make a holistic understanding possible, we discussed this issue's historical, political, educational, and medical aspects. While the issue persists globally, we focused on the US. Due to this fact, we had the chance to talk to three amazing interviewees that advocate for those affected by period poverty and who go even further than merely supporting, but also educating them and everyone else about this important issue. [**Read more...**](https://sts-munich.de/student-voices/menstruation-emancipation/ "Menstruation Emancipation")