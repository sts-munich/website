+++
author = []
categories = ["Short Essays", "Vignettes"]
date = 2022-10-03T23:00:00Z
image = "/uploads/diana-polekhina-1ixt36dfusq-unsplash.jpg"
slug = "vignettes"
tags = ["research note", "essays", "essay"]
title = "Vignettes"

+++
# Vignettes

Welcome to Vignettes, where you can access a collection of assignments submitted by STS and RESET students from their various courses throughout their studies. These vignettes are a valuable resource for understanding the wide-ranging and in-depth knowledge that our students have acquired in their field of study. By exploring the vignettes, you can gain insight into the diverse perspectives, research methods, and critical thinking skills developed by our students. We hope that the vignettes will inspire and inform your own learning journey.

#### Technology as a Silver Bullet

Climate change is one of the most pressing issues facing our world today, and it's a complex problem that requires complex solutions. But as this article argues, relying on technology alone to fix the climate crisis is a delusion. From geo-engineering to afforestation, many so-called "fixes" for climate change may do more harm than good and ignore the root causes of the problem. This article delves into the intricate politics of approving technology on a global scale, and how different solutions can have unexpected consequences on society and power structures. Don't miss this thought-provoking read that explores the problems of quick fixes in tackling climate change, and the hidden complexities of such solutions. [Read More...](https://sts-munich.de/student-voices/tech-as-silver-bullet/ "Tech as silver bullet")

#### Challenging the Deficit Model in Science Communication: Is Uprooting it the Answer?

The knowledge deficit model is a widely discredited concept in science communication that assumes the public is passive and ignorant of science. But despite being challenged by scholars, the model persists among scientists and in policy making. This article delves into the reasons why the deficit model persists, and argues that making science communication education mandatory in graduate STEM programs - as suggested by some - is just another example of the deficit model. The author suggests that instead of trying to completely uproot the deficit model, we should focus on preventing its misuse, and redefining our objectives to better suit the issue at hand. If you're interested in understanding the complexities of the debate surrounding the knowledge deficit model, and the limitations of science communication, this article is a must-read. [Read More...](https://sts-munich.de/student-voices/challenging-the-deficit-model/ "Challenging the Deficit Model")