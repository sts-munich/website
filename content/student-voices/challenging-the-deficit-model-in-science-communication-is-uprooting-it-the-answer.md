+++
author = ["Karl Dagher"]
categories = []
date = 2023-01-11T00:00:00Z
image = "/uploads/pexels-gustavo-fring-6285152.jpg"
slug = "challenging-the-deficit-model"
tags = []
title = "Challenging the Deficit Model in Science Communication: Is Uprooting it the Answer?"

+++
The knowledge deficit model is a diffusionist concept of public communication of science that incorporates a notion of the public as passive and whose “default ignorance and hostility to science can be counteracted by appropriate injection of science communication” (Bucchi & Trench, 2008: 58). This model has been repeatedly challenged by scholars: Brian Wynne, for example, underlines that the deficit model is “more of an ideological construct than a research model” (Wynne, 1993: 322). In his seminal paper, _Public uptake of science: a case for institutional reflexivity_, he demonstrated that “laypeople display considerable reflexive negotiation of their identity in relationships to science” (Wynne, 1993:321) and that the public’s ignorance is not a deficit of knowledge, but rather a “part and parcel of the social construction of social identity” (Wynne, 1993: 327).

So, one would expect that years of scholarship discrediting the deficit model would have led to its death. Instead, the deficit model persists among scientists and elsewhere. In 2016, a group of researchers empirically investigated the reasons why that is so (Simis et al., 2016: 401). They found that scientists’ training, their perceptions of ‘the public’, institutional structures, and the deficit model’s allure for policymaking all played a role in the model’s resilience (Simis et al., 2016: 410). In suggesting fixes that could “uproot” the deficit model (Simis et al., 2016: 400), they suggest making science communication and public engagement part of scientists’ formal training in graduate programs (Simis et al., 2016: 410). I argue that this suggestion is just another idea that stems from the deficit model. The fact that an essay planning the uprooting of the deficit model mobilizes the same deficit model to generate its plan is a telling feature of the model’s resilience. It forces us to reevaluate whether uprooting the deficit model is a realistic objective, and if so, whether it is worth pursuing.

The writers of ‘_The lure of rationality: Why does the deficit model persist in science communication?’_ are of the opinion that if scientists are forced to learn about science communication in their graduate studies, they will change their attitudes towards laypeople and start mobilizing their newly learned methods in the communication of their own work to the public (Simis et al., 2016: 404). One can easily see how this is deficit model thinking par excellence: science communication scholars are the experts, and STEM graduate students are the laypeople (when it comes to science communication). By Simis et al.’s own admission, this is a form of deficit model thinking, but for some reason this admission does not make them consider removing this suggestion from their essay, instead contenting themselves with noting its “irony” (Simis et al., 2016: 404) and hoping that this will absolve the paradoxical argument which presents the ailment as the cure.

Admittedly, the authors do highlight that the effects of making science communication education compulsory in graduate STEM programs will be discernable with scientists who harbor positive attitudes towards the social sciences (Simis et al., 2016: 404). But if this deficit model solution is acceptable in its limited effects, then one questions why it couldn’t be accepted elsewhere, and whether the complete uprooting of the deficit model should still be an objective. If we know the limits of the deficit model as an ideological construct, we can apply surgical fixes to it to transform it into a functional research model whose limitations are clearly defined. Perhaps this would allow the objective to be redefined as the uprooting of the deficit model’s _misuse_. Perhaps this new objective could save us from eternally researching “why the model still persists”.

#### Bibliography

Bucchi, M. & Trench, B., 2008. _Handbook of Public Communication of Science and Technology_, New York, USA: Routledge.

Simis, M.J. et al., 2016. The lure of rationality: Why does the deficit model persist in science communication? _Public Understanding of Science_, 25(4), pp.400–414.

Wynne, B., 1993. Public uptake of science: A case for institutional reflexivity. _Public Understanding of Science_, 2(4), pp.321–337.