---
title: Student Voices
description: Student Voices
image: "/images/student-voices-cover.png"
layout: student-voices

---
Do you want to know more about what students of the STS Department are up to? Then you have come to the right place! Student Voices is a place where students’ creative outbursts such as essays, blog articles, or  podcasts are gathered and shared with the rest world. So, dive in and find out what student life at the STS Department is all about.

***

[**Follow us on Twitter!**](https://twitter.com/sts_voices "twitter")

***

#### [Vignettes](https://sts-munich.de/student-voices/vignettes/ "Vignettes")

Quick research notes, up to 1500 words long, written by STS and RESET students over the course of their studies

#### [Long Reads](https://sts-munich.de/student-voices/long-reads/ "Long Reads")

Essays up to 6000 words long, written by STS and RESET students and submitted to the various courses they take throughout their studies

#### [Podcasts](https://sts-munich.de/student-voices/podcasts/ "Podcasts")

Podcasts produced by RESET students and submitted for Professor Ruth Mueller’s seminar _Telling Responsible Stories_