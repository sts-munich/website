+++
author = ["Karl Dagher", "Salma Atallah", "Nicholas McCay"]
categories = ["Podcasts"]
date = 2023-01-11T00:00:00Z
image = "/uploads/pexels-antony-trivet-12626272-1.jpg"
slug = "power-and-peril-ammonium-nitrate"
tags = ["Podcast"]
title = "The Power & Peril of Dual-Use Technology: Ammonium Nitrate"

+++
On August 4th, 2020, a devastating explosion at the port of Beirut resulted in the loss of lives and widespread damage. The cause of the explosion was traced back to the improper storage of ammonium nitrate, a common ingredient in fertilizers. In this podcast, we delve into the complexities of managing the risks and benefits of ammonium nitrate. Experts in the field of chemical safety and regulations provide their insights and perspective on the tragedy in Beirut and the steps that can be taken to prevent similar incidents in the future.

{{< soundcloud-track 1421774110 >}}