+++
author = ["Student Voices"]
categories = ["The Long Read"]
date = 2022-10-03T23:00:00Z
image = "/uploads/towfiqu-barbhuiya-kkhs1e2yiwc-unsplash.jpg"
slug = "long-reads"
tags = ["final paper", "paper", "essay", "essays"]
title = "Long Reads"

+++
## Long Reads

Every student of the RESET and STS programs generates around three to four essays per semester. Some of them are short vignettes and usually do not exceed 1500 words in length. Others are more in depth, reach 6000 words, and usually require closer empirical work in terms of data collection. Here, you can read these longer essays as submitted by our students.

##### [The Lichenists' Fury: Science and Ideology in the Conflict over Lichen Classification, 1867-1890.](https://sts-munich.de/student-voices/lichenists-fury/ "Lichenists' Fury")

_by Betsy Middleton_

This paper examines the radical political undertones in the mid-19th century scientific debate over lichen taxonomy. As social and political upheaval swept through Europe and the colonies, another revolution was quietly taking place, one that would irreversibly change the way we understand inter-organism relationships and the politics of the natural world.

##### [The Wrath of a Goddess: Bellandur Lake](https://sts-munich.de/student-voices/wrath-of-goddess "Wrath of Goddess")

_by Koushik Ravi Kumar_

The article is a personal reflection on the city of Bengaluru and its rapid urbanization, how it has negatively impacted the natural resources in the city, in particular the Bellandur Lake. The author talks about the history of lakes in Bengaluru and how their value have changed over time, moving from an integral part of rural landscapes to peri-urban ecosystems that require urgent attention. The author illustrates how this transformation in the perceived need has physically transformed the lake itself and raise the alarm to revive and rejuvenate the city’s natural resources. It reflects on the disarray of urban commons in major Indian cities and how it affects the communities around them. The article reflects on the personal experiences of the author and the past, present and future of the city and its resources. The reader will learn about the change in the human perception towards lakes and how it reflects on the wider society.

##### [Navigating the Contested Sociotechnical Imaginaries of Rawabi Tech Hub in Palestine](https://sts-munich.de/student-voices/rawabi-sti/ "Rawabi STI")

_by Chan Ai Ting Melanie_

Palestine represents a unique and challenging setting for innovation to flourish. Occupied by Israel since 1948, it currently represents ‘the last occupied country in the globe to date where entrepreneurs are required to operate in extremely challenging situations’ (Baidoun et al., 2018). The economy of Palestine faces the monolithic challenge of being made largely dependent on Israel for trade and economic opportunities, with a steep decline in growth in recent years due to the occupation’s economic and social restrictions. However, technological entrepreneurship and innovation is perceived as an engine for economic growth worldwide, specifically in developing societies. This article will explore how Palestine's socio-technical imaginaries are entangled in a transnational economy driven by 21st-century global innovation and start-up ecosystems through a case study within an emerging high-tech innovation ecosystem in Palestine.

[The Socio-Technical System of Soybean Extractivism](https://sts-munich.de/student-voices/soybean-extractivism/ "Extractivism")

_by Adam Smoleń_

A thorough analysis of the techno-social patterns of soybean expansion in Brazil using the frameworks of technological momentum and extractivism. 

[Science Education in a Post-truth World: The Controversy Surrounding the Inclusion of Indigenous Knowledge in New Zealand's Science Classes](https://sts-munich.de/student-voices/science-education-in-post-truth/ "Science post-truth")

_by Karl Dagher_