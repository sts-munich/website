+++
author = ["Anna Gagliano", "Elisenda Passola", "Kimberly Savannah Penelope Kreuzer", "Caitlin Kearney"]
categories = ["Telling Responsible Stories", "Podcast"]
date = 2023-01-11T00:00:00Z
image = "/uploads/photo_2023-01-11_10-52-17.jpg"
slug = "menstruation-emancipation"
tags = ["podcast"]
title = "Menstruation Emancipation"

+++
Period Poverty is a global issue that sometimes falls into oblivion even though there are numerous people not only menstruating regularly but also suffering the health and social consequences of this problem. To understand the origins and consequences of period poverty we examined the topic in more depth. To make a holistic understanding possible, we discussed this issue's historical, political, educational, and medical aspects. While the issue persists globally, we focused on the US. Due to this fact, we had the chance to talk to three amazing interviewees that advocate for those affected by period poverty and who go even further than merely supporting, but also educating them and everyone else about this important issue.

{{< soundcloud-track 1413005077 >}}

**Further Reading:**

NoMoreSecrets: [https://www.nomoresecretsmbs.org](https://www.nomoresecretsmbs.org "https://www.nomoresecretsmbs.org")

DaysForGirls: [https://www.daysforgirls.org](https://www.daysforgirls.org "https://www.daysforgirls.org")

FreeThePeriod: [https://freetheperiodca.org](https://freetheperiodca.org "https://freetheperiodca.org")

###### **Support Links for Trigger Warning:**

Global Suicide Hotlines: [https://blog.opencounseling.com/suicide-hotlines/](https://blog.opencounseling.com/suicide-hotlines/ "https://blog.opencounseling.com/suicide-hotlines/")